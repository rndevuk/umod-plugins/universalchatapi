# Universal Chat API

Universal Chat Api is a plugin designed to be used by other plugins, it enables server owners to keep their server messages consistent across plugins.

If this plugin is used on a Rust Server it will also allow the admin to change the Avatar for Server messages too.

This plugin might by extended in the future to do more but at the moment i would like for it to serve one purpose.
At the moment rust is the only game that supports avatar image changes but i will update the plugin to support other games as and when i find out more about the other games.

# Configuration
There isn't much to the configuration in this plugin
```JSON
{
    // The name you want server messages to use.
    "BotName": "<color=#3d83f5>[BOT]</color> Server", 
    // The steam ID containing the avatar to be used in your server messages.
    "BotAvatarId": 0,
	// If set to true, this plugin will hide admin give messages in chat
	"HideGiveMessages": false 
}
```

# Developer API
To call the functions in this API your plugin needs to add the following plugin reference:
```CSHARP
    [PluginReference] Plugin UniversalChatAPI;
```

### - SendMessageToPlayer()
This function and its overloads enable plugins to send a message to a player:
```CSHARP
    UniversalChatAPI?.SendMessageToPlayer(Plugin this, IPlayer player, string message, params object[] args)
```
```CSHARP
    UniversalChatAPI?.SendMessageToPlayer(Plugin this, IPlayer player, string prefix, ulong avatarid, string message, params object[] args)
```
The following 2 functions are Rust specific
```CSHARP
    UniversalChatAPI?.SendMessageToPlayer(Plugin this, BasePlayer player, string message, params object[] args)
```
```CSHARP
    UniversalChatAPI?.SendMessageToPlayer(Plugin this, BasePlayer player, string prefix, ulong avatarid, string message, params object[] args)
```

### - SendMessageToGlobal()
This function and its overloads enable plugins to send a message to all players:
```CSHARP
    UniversalChatAPI?.SendMessageToGlobal(Plugin this, string message, params object[] args)
```
```CSHARP
    UniversalChatAPI?.SendMessageToGlobal(Plugin this, string prefix, ulong avatarid, string message, params object[] args)
```

### - GetBotName()
This function allows you to retreive the configured Name that would replace the default name in server messages.
```CSHARP
    (string)UniversalChatAPI?.GetBotName<string>()
```

### - GetBotavatarid()
This function allows you to retreive the configured Steam ID that is used to replace the default avatar in server messages.
```CSHARP
    (string)UniversalChatAPI?.GetBotavatarid<string>()
```

