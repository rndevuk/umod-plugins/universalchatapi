using Oxide.Core.Libraries.Covalence;
using Oxide.Core.Plugins;
using Newtonsoft.Json;

namespace Oxide.Plugins {

    [Info("Universal Chat Api", "crazyR", "1.0.0")]
    [Description("Universal Chat Api is designed to do 2 things, replace avatar and name for 'SERVER' messages in rust and provide an api for plugins to use when sending messages to players.")]

    class UniversalChatAPI : CovalencePlugin {

        private static ConfigFile _Config;

        #region Umod Hooks
        private void OnServerInitialized() => LoadConfig();

        #if RUST
        // This will be updated to accomodate other games as and when I know more about the other games
        private object OnServerMessage(string message, string name)
        {
            if (name == "SERVER")
            {
                if (_Config.HideGiveMessages == true && message.Contains("gave"))
                    return true;

                SendMessageToGlobal(this, message);
                return true;
            }

            return null;
        }
        #endif
        #endregion

        #region Config
        protected override void LoadConfig()
        {
            base.LoadConfig();

            _Config = Config.ReadObject<ConfigFile>();

            if (_Config == null)
            {
                _Config = new ConfigFile
                {
                    BotName             = "<color=#3d83f5>[BOT]</color> Server",
                    BotAvatarId         = 0,
                    HideGiveMessages    = false
                };

                SaveConfig();
            }
        }

        protected override void SaveConfig() => Config.WriteObject(_Config);

        private class ConfigFile
        {
            [JsonProperty("BotName")]
            public string BotName;

            [JsonProperty("BotAvatarId")]
            public ulong BotAvatarId;

            [JsonProperty("HideGiveMessages")]
            public bool HideGiveMessages;
        }
        #endregion

        #region API
        [HookMethod("GetBotPrefix")]
        private string GetBotName() => _Config.BotName;

        [HookMethod("GetBotavatarid")]
        private ulong GetBotAvatarId() => _Config.BotAvatarId;

        [HookMethod("SendMessageToGlobal")]
        private void SendMessageToGlobal(Plugin plugin, string message, params object[] args) =>
            SendMessageToGlobal(plugin, _Config.BotName, _Config.BotAvatarId, message, args);

        [HookMethod("SendMessageToPlayer")]
        private void SendMessageToPlayer(Plugin plugin, IPlayer player, string message, params object[] args) =>
            SendMessageToPlayer(plugin, player, _Config.BotName, _Config.BotAvatarId, message, args);

        [HookMethod("SendMessageToGlobal")]
        private void SendMessageToGlobal(Plugin plugin, string prefix, ulong avatarid, string message, params object[] args)
        {
            foreach (IPlayer player in players.Connected)
                SendMessageToPlayer(plugin, player, prefix, avatarid, message, args);
        }

        #if RUST
        [HookMethod("SendMessageToPlayer")]
        private void SendMessageToPlayer(Plugin plugin, BasePlayer player, string message, params object[] args) =>
            SendMessageToPlayer(plugin, player, _Config.BotName, _Config.BotAvatarId, message, args);

        [HookMethod("SendMessageToPlayer")]
        private void SendMessageToPlayer(Plugin plugin, BasePlayer player, string prefix, ulong avatarid, string message, params object[] args)
        {
            if (string.IsNullOrEmpty(message))
                return;

            if (lang != null)
                message = lang.GetMessage(message, plugin, player.UserIDString);

            if (args.Length > 0)
                message = string.Format(message, args);

            if (!string.IsNullOrEmpty(prefix))
                message = string.Format("{0}: {1}", prefix, message);

            player.SendConsoleCommand("chat.add", 2, avatarid, Formatter.ToUnity(message));
        }
        #endif

        [HookMethod("SendMessageToPlayer")]
        private void SendMessageToPlayer(Plugin plugin, IPlayer player, string prefix, ulong avatarid, string message, params object[] args)
        {
            #if RUST
            SendMessageToPlayer(plugin, (player.Object as BasePlayer), prefix, avatarid, message, args);
            return;
            #endif

            if (string.IsNullOrEmpty(message))
                return;

            if (lang != null)
                message = lang.GetMessage(message, plugin, player.Id);

            if (string.IsNullOrEmpty(prefix))
                player.Message(message, null, args);
            else
                player.Message(message, prefix+":", args);
        }
        #endregion
    }
}